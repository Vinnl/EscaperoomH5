module.exports = {
  purge: ['./src/**/*.tsx', './src/**/*.jsx', './src/**/*.ts', './src/**/*.js', './src/**/*.md', './src/**/*.mdx'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
