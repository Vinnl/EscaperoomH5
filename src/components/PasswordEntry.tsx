import { ChangeEventHandler, FormEventHandler, useEffect, useState } from "react";

type Props = {
  passwords: string[],
};
export const PasswordEntry: React.FC<Props> = (props) => {
  const [password, setPassword] = useState("");
  const [instructionElement, setInstructionElement] = useState<JSX.Element | null>(null);

  useEffect(() => {
    (async () => {
      if (props.passwords.includes(password)) {
        const InstructionComponent = (await import(`./answers/${password}.mdx`)).default;
        const instructionElement = InstructionComponent ? <InstructionComponent/> : null;
        setInstructionElement(instructionElement);
      } else {
        setInstructionElement(null);
      }

    })();
  }, [password]);

  const updatePassword: ChangeEventHandler<HTMLInputElement> = (e) => {
    e.preventDefault();

    setPassword(e.target.value);
  }

  const onSubmit: FormEventHandler = (e) => {
    e.preventDefault();
  };

  const formClass = instructionElement === null
    ? ""
    : "text-gray-400";
  const instructionClass = instructionElement === null
    ? ""
    : "mt-9 pt-9";

  return (
    <>
      <form onSubmit={onSubmit} className={formClass}>
        <label
          htmlFor="password"
          className="pr-2"
        >Wachtwoord:</label>
        <input
          type="text"
          id="password"
          value={password}
          onChange={updatePassword}
          className="border-gray border-b-2"
        />
      </form>
      <div className={instructionClass}>
        {instructionElement}
      </div>
    </>
  );
}
