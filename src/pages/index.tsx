import Intro from "./intro.mdx";
import { PasswordEntry } from "../components/PasswordEntry";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { readdir } from "fs";
import { resolve } from "path";
import { promisify } from "util";

type Props = InferGetStaticPropsType<typeof getStaticProps>;
const Home: React.FC<Props> = (props) => {
  return (
    <>
      <Intro/>
      <PasswordEntry passwords={props.passwords}/>
    </>
  );
}

export default Home;

export const getStaticProps: GetStaticProps<{ passwords: string[] }> = async (_context) => {
  const answerFilenames = await promisify(readdir)(resolve("src/components/answers/"));
  const passwords = answerFilenames.map(filename => filename.replace(".mdx", ""));

  return {
    props: { passwords }, // will be passed to the page component as props
  }
};
